# Carbon Fields Extended Radio Image Extension

This extension adds a Radio Image field to the Carbon Fields library, with the added ability to retrieve either the label, value, or key/value array of the selected option.

## Installation

To install the extension, you can use the following command:

```shell
composer require mywebsos/carbon-fields-extended-radio-image
```

## Usage

To use the Extended Radio Image field in your Carbon Fields container, you can use the following code:

```php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'post_meta', __( 'Extended Radio Image Field' ) )
    ->add_fields( array(
        Field::make( 'extended_radio_image', 'extended_radio_image_field', __( 'Select image' ) )
            ->set_options( array(
                'sky' => 'https://picsum.photos/200/300',
                'ground' => 'https://picsum.photos/300/400',
            ) )
            ->return_format( 'array' ) // label, array or value ( default )
    ) );
```

You can retrieve the selected option using the following methods:

```php
// Retrieve the label of the selected options
$value = carbon_get_post_meta( get_the_ID(), $field_name );
```

## Note

Make sure to include the use statement of Carbon_Fields\Field at the top of your file.
