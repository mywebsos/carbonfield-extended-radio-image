/**
 * External dependencies.
 */
import RadioField from "../vendor/htmlburger/carbon-fields/packages/core/fields/radio-image";

class ExtendedRadioImageField extends RadioField {}

export default ExtendedRadioImageField;
