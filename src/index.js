/**
 * External dependencies.
 */
import { registerFieldType } from "@carbon-fields/core";

/**
 * Internal dependencies.
 */
import "./style.scss";
import ExtendedRadioImageField from "./main";

registerFieldType("extended_radio_image", ExtendedRadioImageField);
