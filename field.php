<?php

use Carbon_Fields\Carbon_Fields;
use Carbon_Field_Extended_Radio_Image\Extended_Radio_Image_Field;

define( 'Carbon_Field_Extended_Radio_Image\\DIR', __DIR__ );

Carbon_Fields::extend( Extended_Radio_Image_Field::class, function( $container ) {
	return new Extended_Radio_Image_Field(
		$container['arguments']['type'],
		$container['arguments']['name'],
		$container['arguments']['label']
	);
} );
